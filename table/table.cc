#include "table.h"
#include "ui_table.h"
#include <QTableWidget>

#include <iostream>
using namespace std;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->spinBox->setMinimum (1);
    ui->spinBox->setMaximum (MAX);
    ui->spinBox->setValue (N);

    ui->checkBox->setChecked (true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

QColor MainWindow::columnColor (int k)
{
    return QColor::fromHsl (k*360/N, 220, 120);
}

void MainWindow::display ()
{
    QTableWidget * table = new QTableWidget (ui->tabWidget);
    int cnt = ui->tabWidget->count ();
    ui->tabWidget->addTab (table, QString::number (cnt+1));

    table->setColumnCount (N);
    table->setRowCount (N);

    int w = table->lineWidth();
    for (int i = 0; i < N; i++) // k .. cislo sloupce
        table->setColumnWidth(i, w);

    for (int k = 0; k < N; k++) // k .. cislo sloupce
    {
        int i = a[k]; // i .. cislo radky
        QTableWidgetItem * item = new QTableWidgetItem;
        item->setText (QString::number (k+1));
        item->setToolTip (QString::number (i) + "," + QString::number (k));
        // item->setForeground (QColor ("red"));
        // item->setBackgroundColor (QColor ("yellow").lighter());
        item->setBackgroundColor (columnColor (k));
        table->setItem (i, k, item);
    }
}

void MainWindow::place (int k)// umistovat damu na k-ty sloupce
{
    for (int i = 0; i < N; i++) // i .. cislo radky
    {
        bool ok = true;
        for (int v = 1; v <= k && ok; v ++)
            if (a[k-v] == i || a[k-v] == i-v || a[k-v] == i+v)
                ok = false;

        if (ok)
        {
            a[k] = i;
            if (k < N-1)
            {
                place (k+1);
            }
            else
            {
                results ++;
                if (disp) display ();
            }
        }
    }
}

void MainWindow::on_actionRun_triggered()
{
    N = ui->spinBox->value();
    disp = ui->checkBox->isChecked();
    results = 0;

    ui->tabWidget->clear ();
    for (int i = 0; i < N; i++) a[i] = -1;
    place (0);

    ui->lineEdit->setText (QString::number (results));
    ui->slider->setMaximum (results-1);
}

void MainWindow::on_slider_sliderMoved(int pos)
{
    int cnt = ui->tabWidget->count();
    if (pos >= 0 && pos <= cnt -1)
        ui->tabWidget->setCurrentIndex (pos);
}

void MainWindow::on_actionQuit_triggered()
{
    close ();
}

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}

