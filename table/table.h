#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

const int MAX = 16;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    int N = 8; // 1 <= N <= MAX
    int a [MAX]; // a[i] ... cislo radky, kde je dama na i-tem sloupci

    bool disp;
    int results;
    QColor columnColor (int k);

    void display ();
    void place (int k); // umistovat damu na k-ty sloupce

private slots:
    void on_actionRun_triggered();

    void on_actionQuit_triggered();

    void on_slider_sliderMoved(int position);

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
