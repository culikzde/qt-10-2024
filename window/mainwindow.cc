#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QApplication>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    // ui->pushButton->setText ("Click");
    bool b = ui->checkBox->isChecked ();

    if (b)
    {
        ui->textEdit->setTextColor(QColor (0, 255, 0));
        ui->textEdit->append ("zapnuto");
    }
    else
    {
        ui->textEdit->setTextColor(QColor ("red"));
        ui->textEdit->append ("vypnuto");
    }
    ui->textEdit->setTextColor(QColor ("black"));

    int k = ui->spinBox->value ();
    ui->textEdit->append ("k = " + QString::number (k));
    ui->spinBox->setValue (k+1);

    ui->checkBox->setChecked(! b);

    QString s = ui->lineEdit->text ();
    ui->textEdit->append ("s = " + s);

    ui->textEdit->append ("");
}

void MainWindow::on_pushButton_pressed()
{
    ui->pushButton->setText ("Stisknuto");

}

void MainWindow::on_pushButton_released()
{
    ui->pushButton->setText ("Uvolneno");

}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
