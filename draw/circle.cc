#include "circle.h"
#include "block.h"
#include "toolbutton.h"
#include <QPen>
#include <QBrush>
#include <QMimeData>
#include <QGraphicsSceneDragDropEvent>

Circle::Circle()
{
    setRect (0, 0, 40, 40);
    setPen (QColor ("blue"));
    setBrush (QColor ("cornflowerblue"));
    setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);

    #ifdef DRAG_AND_DROP
    setAcceptDrops (true); // <--
    #endif
}

#ifdef DRAG_AND_DROP
void Circle::dragEnterEvent (QGraphicsSceneDragDropEvent * event)
{
    const QMimeData * mime = event->mimeData ();
    if (mime->hasColor() || mime->hasFormat (toolFormat))
        setAcceptDrops (true);
    else
        setAcceptDrops (false);
}

void Circle::dropEvent (QGraphicsSceneDragDropEvent * event)
{
    const QMimeData * mime = event->mimeData ();
    if (mime->hasColor ())
    {
        modifyColor (this, event);
    }
    else if (mime->hasFormat (toolFormat))
    {
        addObject (this, event);
    }
}
#endif
