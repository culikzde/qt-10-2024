
/* socket.cc */

#include "socket.h"
#include "jsonio.h"

#include <QTcpSocket>
// #include <iostream>

SocketReceiver::SocketReceiver (Win * win_param, int port_param) :
   QObject (win_param->getMainWindow()),
   win (win_param),
   info (win_param->getInfo ()),
   port (port_param)
{
   run ();
}

SocketReceiver::~SocketReceiver()
{
    // std::cerr << "SocketReciever destroyed" << std::endl;
}

void SocketReceiver::print (QString s)
{
   if (info != nullptr)
      info->append (s);
}

static QByteArray readFile (QString fileName)
{
    QByteArray result;
    QFile file (fileName);
    if (file.open (QIODevice::ReadOnly))
    {
       result = file.readAll ();
       file.close ();
    }
    return result;
}

void SocketReceiver::connection ()
{
    QTcpSocket * socket = server->nextPendingConnection();
    connect (socket, &QTcpSocket::disconnected, socket, &QObject::deleteLater);

    // print ("New connection");
    print ("New connection from " + socket->peerName() + " port " + QString::number (socket->peerPort()));

    while (!(socket->waitForReadyRead (100))) { } // <-- important, waiting for data to be read from web browser
    QByteArray request = socket->readAll();
    QList<QByteArray> lines = request.split ('\n');
    QList<QByteArray> words = lines[0].split (' ');

    QByteArray method = words[0];
    QByteArray path = words [1];
    print (method + " " + path);
    print ("REQUEST\r\n" + request);

    QByteArray block = "";
    block += "HTTP/1.1 200 OK\r\n";

    if (false)
    {
        block += "Content-Type: text/html\r\n";
        block += "\r\n";
        block += "Hello from Qt\r\n";
    }

    if (method == "GET" && path == "/")
    {
        block += "Content-Type: text/html\r\n";
        block += "\r\n";
        block += "<!DOCTYPE html>\r\n";
        block += "<html>\r\n";
        block += "   <head>\r\n";
        block += "      <title>Local</title>\r\n";
        block += "   </head>\r\n";
        block += "   <body>\r\n";
        block += "       Hello from Qt\r\n";
        block += "       <p>";
        block += "       <a href='draw.html'>draw<a>";
        block += "       <p>";
        block += "       <a href='easyui.html'>easyui<a>";
        block += "       <p>";
        block += "       <a href='data.json'>data<a>";
        block += "   </body>\r\n";
        block += "</html>\r\n";
        block += "\r\n";
    }

    if (method == "GET" && path == "/data.json")
    {
        block += "Content-Type: application/json\r\n";
        block += "\r\n";

        // create sting with json data
        QByteArray code = writeJson (win->getScene());
        block += code;
    }

    if (method == "GET" && path == "/easyui.html")
    {
        block += "Content-Type: text/html\r\n";
        block += "\r\n";
        block += readFile (":/data/easyui.html");
    }

    if (method == "GET" && path == "/draw.html")
    {
       block += "Content-Type: text/html\r\n";
       block += "\r\n";
       block += readFile (":/data/draw.html");
    }

    if (method == "GET" && path == "/chatclient.html")
    {
       block += "Content-Type: text/html\r\n";
       block += "\r\n";
       block += readFile (":/data/chatclient.html");
    }

    if (method == "GET" && path == "/client.html")
    {
       block += "Content-Type: text/html\r\n";
       block += "\r\n";
       block += readFile (":/data/client.html");
    }

    if (method == "GET" && path == "/tree.json")
    {
        block += "Content-Type: application/json\r\n";
        block += "\r\n";
        block += readFile (":/data/tree.json");
    }

    #if 0
    if (method == "GET" && path == "/table.json")
    {
        block += "Content-Type: application/json\r\n";
        block += "\r\n";
        block += readFile (":/data/table.json");
    }
    #else
    if (method == "GET" && path == "/table.json")
    {
        block += "Content-Type: application/json\r\n";
        block += "\r\n";

        QJsonArray array;
        #if 0
           QJsonObject obj;
           obj ["name"] = "NAME";
           obj ["type"] = "TYPE";
           array.append (obj);
        #else
        for (QGraphicsItem * item : win->getScene()->items (Qt::AscendingOrder))
        {
            // QJsonObject obj;
            // obj ["name"] = item->toolTip ();
            QJsonObject obj = writeItem (item);
            array.append (obj);
        }
        #endif

        QJsonObject answer;
        answer ["total"] = array.count();
        answer ["rows"] = array;
        QJsonDocument doc (answer);
        block += doc.toJson ();

        print ("RESPONSE\r\n" + block);
    }
    #endif

    socket->write (block);

    socket->flush ();
    socket->waitForBytesWritten (3000);

    socket->disconnectFromHost();
}

void SocketReceiver::run()
{
    server = new QTcpServer (this);
    connect (server, &QTcpServer::newConnection, this, &SocketReceiver::connection);
    bool ok = server->listen (QHostAddress::Any, port);
    if (ok)
       print ("Listening on port " + QString::number (port));
    else
       print ("Error when listening");
}

/* ---------------------------------------------------------------------- */

// http://doc.qt.io/qt-5/qtnetwork-fortuneserver-example.html

// http://www.bogotobogo.com/cplusplus/sockets_server_client.php
// http://www.bogotobogo.com/Qt/Qt5_QTcpServer_Client_Server.php
// http://www.bogotobogo.com/Qt/Qt5_QTcpServer_Multithreaded_Client_Server.php
// http://www.bogotobogo.com/Qt/Qt5_QTcpServer_QThreadPool_Multithreaded_Client_Server.php

// http://stackoverflow.com/questions/3122508/qt-http-server  "answer: Here is a very simple HTTP web server"

// http://github.com/qt-labs/qthttpserver/blob/master/src/httpserver/qabstracthttpserver.cpp
