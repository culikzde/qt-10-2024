#include "draw.h"
#include "ui_draw.h"
#include <QApplication>
#include <QAbstractGraphicsShapeItem>
#include <QGraphicsRectItem>
#include <QBitmap>

#include "block.h"
#include "circle.h"
#include "port.h"

#include "colorbutton.h"
#include "toolbutton.h"
#include "scene.h"
#include "tree.h"
#include "property.h"

#include "io.h"
#include "xmlio.h"
#include "jsonio.h"

#include "qt-view.h"

#ifdef USE_SQL
   #include "db.h"
#endif

#ifdef USE_DBUS
   #include "dbus.h"
#endif

#ifdef USE_JS
   #include "js.h"
#endif

#ifdef USE_PYTHON
   #include "python-engine.h"
#endif

#ifdef USE_INVENTOR
   #include "inventor.h"
#endif

#ifdef USE_SOCKET
   #include "socket.h"
#endif

// #ifdef QT_SVG_LIB
//    #define USE_SVG
// #endif

#ifdef USE_SVG
   #include "svg.h"
#endif

#ifdef USE_WEB_KIT
  #define USE_WEB
#endif

#ifdef USE_WEB_ENGINE
  #define USE_WEB
#endif

#ifdef USE_WEB
   #include "html.h"
#endif

#ifdef USE_WEB_CHANNEL
   #include "webchannel.h"
#endif

#ifdef USE_PANEL
   #include "panel.h"
#endif

Win * global_win = nullptr;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    global_win = this;
    ui->setupUi(this);
    info = ui->info;

    ui->vsplitter->setStretchFactor (0, 4); // 4 dily
    ui->vsplitter->setStretchFactor (1, 1); // 1 dil

    ui->hsplitter->setStretchFactor (0, 1);
    ui->hsplitter->setStretchFactor (1, 3);
    ui->hsplitter->setStretchFactor (2, 1);

    ui->leftTools->setTabPosition (QTabWidget::West);
    ui->rightTools->setTabPosition (QTabWidget::East);

    ui->statusbar->showMessage (QString ("Qt ") + qVersion());

    createPalette ();

    /* scene */

    scene = new Scene (this);
    ui->graphicsView->setScene (scene);
    // drawBoard (scene);

    // scene->addLine (40, 40, 200, 100, QColor ("red"));
    scene->addItem (new Block (true));

    /* tree */

    tree = new Tree (this);
    tree->setScene (scene);
    tree->setWin (this);
    addLeftTool (tree, "Tree");
    connect (tree, &Tree::itemSelectionChanged, this, &MainWindow::on_tree_itemSelectionChanged);
    connect (tree, &Tree::itemDoubleClicked, this, &MainWindow::on_tree_itemDoubleClicked);
    refreshTree ();

    /* property table */

    prop = new PropertyTable (this);
    prop->setWin (this);
    addRightTool (prop, "Properties");

    /* database */

    #ifdef USE_SQL
       QTreeWidget * db_tree = new QTreeWidget (this);
       addLeftTool (db_tree, "SQL");
       addTab (new DbView (this, scene, db_tree, info), "SQL");
    #endif

    /* socket */

    #ifdef USE_SOCKET
       new SocketReceiver (this, 1234);
    #endif

    /* web view */

    #ifdef USE_WEB
       addTab (new HtmlView (this), "HTML");
    #endif

    /* dbus */

    #ifdef USE_DBUS
       new Receiver (this, info, scene);
    #endif

    /* java script */

    #ifdef USE_JS
       js_tree = new QTreeWidget (this);
       addLeftTool (js_tree, "JS");

       js_edit = new JsEdit (this, js_tree, info, scene);
       addRightTool (createRunBox (js_edit, ui->actionScript), "JS");

       showTool (js_edit);
    #endif

    /* python */

    #ifdef USE_PYTHON
       python_edit = new QTextEdit (this);
       addRightTool (createRunBox (python_edit, ui->actionPython), "Python");
       // showTool (python_edit);
    #endif

    /* Open Inventor */

    #ifdef USE_INVENTOR
       inventor_tree = new Tree (this);
       connect (inventor_tree, &Tree::itemDoubleClicked, this, &MainWindow::on_inventor_tree_clicked);

       addLeftTool (inventor_tree, "Inventor");
       inventorScene (inventor_tree, ui->tabs, prop);

       // ui->tabs->setCurrentIndex (ui->tabs->count () - 1);
    #endif

    /* panels */

    #ifdef USE_PANEL
       #ifdef BUILDER_PANEL
          addRightTool (new BuilderWindow (this), "Builder");
       #endif
       #ifdef VISUALIZATION_PANEL
           addTab (createVisualizationWindow (this), "Visual");
       #endif
       #ifdef QT3D_PANEL
          addTab (createQt3DWindow (this), "QT3D");
       #endif
       ui->tabs->setCurrentIndex (0);
    #endif

    /* qt classes */

    qt_tree = new QTreeWidget (this);
    addLeftTool (qt_tree, "QT");
    display_qt_widgets (qt_tree);
    display_qt_classes (qt_tree);

    /* actions */

    #ifndef USE_SVG
       ui->actionSvg->setEnabled (false);
       // ui->actionSvg->setVisible (false);
    #endif

    #ifndef USE_DBUS
       ui->actionCall->setEnabled (false);
    #endif

    #ifndef USE_JS
       ui->actionScript->setEnabled (false);
    #endif

    #ifndef USE_PYTHON
       ui->actionPython->setEnabled (false);
    #endif

   #ifndef USE_WEB_CHANNEL
      ui->actionWebChannel->setVisible (false);
   #endif
}

MainWindow::~MainWindow()
{
    delete ui;
}

QMainWindow * MainWindow::getMainWindow () { return this; }
QStatusBar * MainWindow::getStatusBar () { return statusBar(); }
Tree * MainWindow::getTree () { return tree; }
PropertyTable * MainWindow::getPropTable () { return prop; }
QTextEdit * MainWindow::getInfo () { return info; }
QGraphicsScene * MainWindow::getScene () { return scene; };

void MainWindow::createPalette ()
{
    ui->palette->clear ();

    /* colors */

    QToolBar * page = new QToolBar (this);
    ui->palette->addTab (page, "Colors");

    QStringList names = { "red", "green", "blue", "yellow", "orange",
                          "silver", "gold", "goldenrod",
                          "lime", "lime green", "yellow green", "green yellow",
                          "forest green", "coral", "cornflower blue", "dodger blue",
                          "royal blue", "wheat", "chocolate", "peru", "sienna", "brown" };

    for (auto name : names)
    {
       ColorButton * b = new ColorButton (name);
       page->addWidget (b);
    }

    /* tools */

    page = new QToolBar (this);
    ui->palette->addTab (page, "Tools");
    ui->palette->setCurrentWidget (page);

    names.clear ();
    names << "line" << "rectangle"<<  "ellipse" ;
    for (auto name : names)
    {
       page->addWidget (new ToolButton (name, toolFormat));
    }

    #ifdef USE_PANEL
    for (QString name : componentTools ())
    {
        page->addWidget (new ToolButton (name, toolFormat));
    }
    #endif

    #ifdef USE_PANEL
       QWidget * widgetTab = new QWidget (ui->palette);
       ui->palette->addTab (widgetTab, "Widgets");

       QToolBar * widgetBar = new QToolBar (widgetTab);
       addWidgetButtons (widgetBar);
       // ui->palette->setCurrentIndex (2);
    #endif
}

void MainWindow::addTab (QWidget * widget, QString title)
{
    int inx = ui->tabs->addTab (widget, title);
    ui->tabs->setCurrentIndex (inx);
}

void MainWindow::addLeftTool (QWidget * widget, QString title)
{
    ui->leftTools->addTab (widget, title);
}

void MainWindow::addRightTool (QWidget * widget, QString title)
{
    ui->rightTools->addTab (widget, title);
}

void MainWindow::showTool (QWidget * widget)
{
    int inx = ui->leftTools->indexOf (widget);
    if (inx >= 0) ui->leftTools->setCurrentIndex (inx);

    inx = ui->rightTools->indexOf (widget);
    if (inx >= 0) ui->rightTools->setCurrentIndex (inx);

    inx = ui->tabs->indexOf (widget);
    if (inx >= 0) ui->tabs->setCurrentIndex (inx);
}

QWidget * MainWindow::createRunBox (QWidget * widget, QAction * action)
{
   QWidget * result = new QWidget (this);
   QVBoxLayout * layout = new QVBoxLayout (this);

   layout->addWidget (widget);

   QPushButton * button = new QPushButton ("Run", this);
   connect (button, &QPushButton::clicked, action, &QAction::triggered);
   layout->addWidget (button);

   result->setLayout (layout);
   return result;
}

void MainWindow::showProperty (QString name, QVariant value)
{
    prop->addText (name, value.toString ()); // !?
}

void MainWindow::showProperties (QGraphicsItem * item)
{
    prop->displayProperties (item);
}

void MainWindow::showInfo (QString s)
{
    info->append (s);
}

void MainWindow::on_actionTree_triggered()
{
    tree->displayTree ();
    showTool (tree);
}

void MainWindow::on_actionSvg_triggered()
{
    #ifdef USE_SVG
       addTab(new SvgView (this), "SVG");
    #endif
}

/* Tree */

void MainWindow::refreshTree ()
{
    tree->displayTree ();
}

void MainWindow::refreshTreeName (QGraphicsItem * item, QString name)
{
    tree->renameTreeItem (item, name);
}

void MainWindow::refreshNewTreeItem (QGraphicsItem * item)
{
   tree->addTreeItem (item);
}

void MainWindow::on_inventor_tree_clicked (QTreeWidgetItem * node, int column)
{
    TreeItem * tree_node = dynamic_cast <TreeItem *> (node);
    #ifdef USE_INVENTOR
    if (tree_node != nullptr && tree_node->so_node != nullptr)
       showSoNode (prop, tree_node->so_node);
    #endif
}

void MainWindow::on_tree_itemDoubleClicked (QTreeWidgetItem * node, int column)
{
    TreeItem * tree_node = dynamic_cast <TreeItem *> (node);
    if (tree_node != nullptr && tree_node->graphics_item != nullptr)
       showProperties (tree_node->graphics_item);
    else
       showProperties (nullptr);
}

void MainWindow::on_tree_itemSelectionChanged ()
{
    QGraphicsItem * item = nullptr;
    QList <QTreeWidgetItem * > list = tree->selectedItems ();
    if (list.count () == 1)
    {
       TreeItem * node = dynamic_cast < TreeItem * > (list[0]);
       if (node != nullptr)
          item = node->graphics_item;
    }
    showProperties (item);
}

/* Clipboard */

void MainWindow::on_actionCopy_triggered ()
{
    QString code = "";
    QXmlStreamWriter writer (& code);
    writeBegin (writer);

    for (QGraphicsItem * item : scene->selectedItems ())
    {
        writeItem (writer, item);
    }

    writeEnd (writer);

    QMimeData * data = new QMimeData;
    data->setData (shapeFormat, code.toLatin1 ());

    QClipboard * clip = QApplication::clipboard ();
    clip->setMimeData (data);
}

void MainWindow::on_actionPaste_triggered ()
{
    QClipboard * clip = QApplication::clipboard ();
    const QMimeData * data = clip->mimeData ();
    if (data->hasFormat (shapeFormat))
    {
       QString code = data->data (shapeFormat);
       QXmlStreamReader reader (code);
       readXml (reader, scene);
    }
    refreshTree ();
}

/* Load / Save */

void MainWindow::loadFile (QString fileName, bool json)

{
    /*
    QFile f (fileName);
    if (f.open (QFile::ReadOnly))
    {
        QXmlStreamReader r (&f);
        readXml (r, scene);
    }
    refreshTree ();
    */

    QFile f (fileName);
    if (f.open (QFile::ReadOnly))
    {
        if (json)
        {
            QByteArray code = f.readAll ();
            readJson (code, scene);
        }
        else
        {
           QXmlStreamReader r (&f);
           readXml (r, scene);
        }
        refreshTree ();
    }
    else
    {
       QMessageBox::warning (this, "Open File Error", "Cannot read file: " + fileName);
    }
}

void MainWindow::saveFile (QString fileName, bool json)

{
    /*
    QFile f (fileName);
    if (f.open (QFile::WriteOnly))
    {
        QXmlStreamWriter w (&f);
        writeXml (w, scene);
    }
    */

    QFile f (fileName);
    if (f.open (QFile::WriteOnly))
    {
        if (json)
        {
            QByteArray code = writeJson (scene);
            f.write (code);
        }
        else
        {
           QXmlStreamWriter w (&f);
           writeXml (w, scene);
        }
    }
    else
    {
       QMessageBox::warning (this, "Save File Error", "Cannot write file: " + fileName);
    }
}

const QString dir = QString ();
const QString filter = "Json files(*.json);;XML files(*.xml)";
// const QString jsonFilter = "Json files";

void MainWindow::on_actionOpen_triggered ()
{
    /*
    QString fileName = QFileDialog::getOpenFileName (this, "Open file");
    if (fileName != "")
       loadFile (fileName, false);
    */

    QString selectedFilter;
    QString fileName = QFileDialog::getOpenFileName (this, "Open file", dir, filter, &selectedFilter);
    if (fileName != "")
        // loadFile (fileName, selectedFilter.startsWith (jsonFilter));
        loadFile (fileName, fileName.endsWith (".json"));
}

void MainWindow::on_actionSave_triggered ()
{
    /*
    QString fileName = QFileDialog::getSaveFileName (this, "Save file");
    if (fileName != "")
        saveFile (fileName);
    */
    QString selectedFilter;
    QString fileName = QFileDialog::getSaveFileName (this, "Save file", dir, filter, &selectedFilter);
    if (fileName != "")
       saveFile (fileName, fileName.endsWith (".json"));
}

void MainWindow::on_actionTransfer_triggered ()
{
   #ifdef USE_PANEL
       transferData ();
   #endif
}

void MainWindow::on_actionCall_triggered ()
{
   #ifdef USE_DBUS
       callHello ("Hello from Qt");
   #endif
}

void MainWindow::on_actionScript_triggered ()
{
   #ifdef USE_JS
       // run_script (scene, edit, info);
       showTool (js_tree);
       showTool (js_edit);
       js_edit->run ();
   #endif
}

void MainWindow::on_actionPython_triggered ()
{
   #ifdef USE_PYTHON
       showTool (python_edit);
       QString cmd = python_edit->toPlainText ();
       cmd = cmd + "\nimport sys; sys.stderr.flush()";
       run_python_string (cmd.toStdString ());
   #endif
}

void MainWindow::on_actionQuit_triggered ()
{
    close ();
}

int main (int argc, char *argv[])
{
    QApplication appl (argc, argv);

    #ifdef USE_INVENTOR
       SoQt::init ((QWidget*)NULL); // <-- important
    #endif

    #ifdef USE_PYTHON
       init_python_module ();
       add_sample_module ();
       test_sample_module ();
    #endif

    #ifdef USE_WEB_CHANNEL
       init_chat_server (appl, 12345);
    #endif

    MainWindow * win = new MainWindow;
    win->show ();

    int code = appl.exec ();

    #ifdef USE_PYTHON
       finish_python_module ();
    #endif

    return code;
}
