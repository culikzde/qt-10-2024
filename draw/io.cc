#include "io.h"

#include "block.h"
#include "circle.h"
#include "port.h"

#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsLineItem>
#include <QPen>

#ifdef USE_PANEL
   #include "panel.h"
#endif

/* ---------------------------------------------------------------------- */

QMap <QString, QString> color_map;

void initColorMap ()
{
    for (QString name : QColor::colorNames())
    {
        QColor c (name);
        QString txt = c.name ();
        color_map [txt] = name;
    }
}

QString colorToString (QColor c)
{
    QString txt = c.name ();
    if (color_map.contains (txt))
        return color_map [txt];
    else
        return txt;
}

QString penToString (QPen p)
{
    return colorToString (p.color());
}

QString brushToString (QBrush b)
{
    return colorToString (b.color());
}

QColor stringToColor (QString name, QString default_name)
{
    if (QColor::isValidColor (name))
       return QColor (name);
    else
       return QColor (default_name);
}

/* ---------------------------------------------------------------------- */

QString itemType (QGraphicsItem * item)
{
    QString result = "node";
    if (dynamic_cast < Block * > (item) != nullptr)
       result = "block";
    else if (dynamic_cast < Circle * > (item) != nullptr)
       result = "circle";
    else if (dynamic_cast < Source * > (item) != nullptr)
       result = "source";
    else if (dynamic_cast < Target * > (item) != nullptr)
       result = "target";
    else if (dynamic_cast < ConnectionLine * > (item) != nullptr)
       result = "connection";
    else if (dynamic_cast < QGraphicsRectItem * > (item) != nullptr)
       result = "rectangle";
    else if (dynamic_cast < QGraphicsEllipseItem * > (item) != nullptr)
       result = "ellipse";
    else if (dynamic_cast < QGraphicsPolygonItem * > (item) != nullptr)
       result = "polygon";
    else if (dynamic_cast < QGraphicsLineItem * > (item) != nullptr)
       result = "line";
    #if USE_PANEL
    else if (Panel * panel = dynamic_cast < Panel * > (item))
       result = panel->typeName ();
    #endif

    return result;
}

/* ---------------------------------------------------------------------- */

QGraphicsItem * createItem (QString type)
{
    QGraphicsItem * result = nullptr;

    if (type == "block")
        result = new Block;
    else if (type == "circle")
        result = new Circle;

    else if (type == "source")
        result = new Source ();
    else if (type == "target")
        result = new Target;
    else if (type == "connection")
        result = new ConnectionLine;


    else if (type == "rectangle")
        result = new QGraphicsRectItem;
    else if (type == "ellipse")
        result = new QGraphicsEllipseItem;
    else if (type == "polygon")
        result = new QGraphicsPolygonItem;
    else if (type == "line")
        result = new QGraphicsLineItem;

    #ifdef USE_PANEL
    else
    {
       result = createComponent (type);
    }
    #endif

    return result;
}

/* ---------------------------------------------------------------------- */

void setupItem (QGraphicsItem * item)
{
    item->setFlag (QGraphicsItem::ItemIsMovable);
    item->setFlag (QGraphicsItem::ItemIsSelectable);
}

/* ---------------------------------------------------------------------- */

QGraphicsItem * itemFromTool (QString tool, int x, int y)
{
    QGraphicsItem * result = nullptr;

    if (tool == "rectangle")
    {
        /*
        QGraphicsRectItem * item = new QGraphicsRectItem;
        item->setRect (0, 0, 100, 80);
        item->setPen (QColor ("blue"));
        item->setBrush (QColor ("yellow"));
        item->setToolTip ("rectangle");
        item->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsEllipseItem::ItemIsSelectable);
        result = item;
        */
        result = new Block ();
    }
    else if (tool == "ellipse")
    {
        /*
        QGraphicsEllipseItem * item = new QGraphicsEllipseItem;
        item->setRect (0, 0, 60, 40);
        item->setPen (QColor ("orange"));
        item->setBrush (QColor ("lime"));
        item->setToolTip ("ellipse");
        result = item;
        */
        result = new Circle ();
    }
    else if (tool == "line")
    {
        /*
        QGraphicsLineItem * item = new QGraphicsLineItem;
        item->setLine (0, 0, 60, 40);
        item->setPen (QColor ("red"));
        item->setToolTip ("line");
        result = item;
        */
        result = createConnection ();
    }

    if (result != nullptr)
    {
       result->setPos (x, y);
       result->setFlags (QGraphicsItem::ItemIsMovable  | QGraphicsEllipseItem::ItemIsSelectable);
    }
    return result;
}

/* ---------------------------------------------------------------------- */

void add (FieldList & list, QString name, QVariant value)
{
    Field field;
    field.name = name;
    field.value = value;
    list.append (field);
}

FieldList getFields (QGraphicsItem * item)
{
    FieldList list;
    if (item != nullptr)
    {
       add (list, "type", itemType (item));
       add (list, "name", item->toolTip ());

       add (list, "x", item->x());
       add (list, "y", item->y());

       if (QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
       {
           add (list, "pen", penToString (shape->pen()));
           add (list, "brush", brushToString (shape->brush()));

           if (QGraphicsRectItem * e = dynamic_cast < QGraphicsRectItem * > (shape))
           {
              add (list, "width",  e->rect().width());
              add (list, "height", e->rect().height());
           }

           if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
           {
              add (list, "width",  e->rect().width());
              add (list, "height", e->rect().height());
           }

          if (QGraphicsLineItem * e = dynamic_cast < QGraphicsLineItem * > (shape))
          {
               add (list, "width", e->line().dx());
               add (list, "height", e->line().dy());
           }
       }

    }
    return list;
}

/* ---------------------------------------------------------------------- */

void setFields (QGraphicsItem * item, FieldList & list)
{
    QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item);
    QGraphicsRectItem * r = dynamic_cast < QGraphicsRectItem * > (item);
    QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (item);
    QGraphicsLineItem * line = dynamic_cast < QGraphicsLineItem * > (item);

    for (Field field : list)
    {
        QString name = field.name;
        QVariant value = field.value;

        if (name == "name")
        {
           item->setToolTip (value.toString ());
        }

        if (name == "x")
        {
           int x = value.toInt ();
           int y = item->pos().y();
           item->setPos (x, y);
        }

        if (name == "y")
        {
           int x = item->pos().x();
           int y = value.toInt ();
           item->setPos (x, y);
        }

       if (name == "pen")
       {
          if (shape)
             shape->setPen (QColor (value.toString()));
       }

       if (name == "brush")
       {
          if (shape)
             shape->setBrush (QColor (value.toString()));
       }

       if (name == "width")
       {
          if (r)
          {
              int w = value.toInt ();
              int h = r->rect().height ();
              r->setRect (0, 0, w, h);
          }

          if (e)
          {
              int w = value.toInt ();
              int h = e->rect().height ();
              e->setRect (0, 0, w, h);
          }

          if (line)
          {
              int w = value.toInt ();
              int h = line->line().y2 ();
              line->setLine (0, 0, w, h);
          }
       }

       if (name == "height")
       {
          if (r)
          {
              int w = r->rect().width ();
              int h = value.toInt ();
              r->setRect (0, 0, w, h);
          }

          if (e)
          {
              int w = e->rect().width ();
              int h = value.toInt ();
              e->setRect (0, 0, w, h);
          }

          if (line)
          {
              int w = line->line().x2 ();
              int h = value.toInt ();
              line->setLine (0, 0, w, h);
          }
       }
    }
}

/* ---------------------------------------------------------------------- */
/* ---------------------------------------------------------------------- */

int shape_cnt = 0;
// QHash <QString, QAbstractGraphicsShapeItem *> all_shapes;

QString modifyName (QString s)
{
    QString name = "";

    for (QChar c : s)
       if (c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c >= '0' && c <= '9' || c == '_')
          name += c;

    if (name == "")
    {
        shape_cnt ++;
        name = "shape" + QString::number (shape_cnt);
    }
    else if (name [0] >= '0' && name [0] <= '9')
    {
        name = "shape_" + name;
    }

    return name;
}

QString getShapeName (QAbstractGraphicsShapeItem * shape)
{
    return modifyName (shape->toolTip ());
}

#if 0
QString getGraphicsItemName (QGraphicsItem * item)
{
   QString name = "";
   if (QObject * obj = dynamic_cast < QObject * > (item))
   {
      name = modifyName (obj->objectName ());
   }
   else if (QAbstractGraphicsShapeItem * shape = dynamic_cast <QAbstractGraphicsShapeItem *> (item))
   {
      s = modifyName (obj->toolTip ());
   }
   return name;
}
#endif

QString ShapeObject::getName ()
{
    return getShapeName (shape);
}

/* ---------------------------------------------------------------------- */
