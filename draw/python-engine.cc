
/* python-engine.cc */

#include "python-engine.h"

#undef socket
#undef slots
#include <Python.h>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h> // string and vector parameter conversions
namespace py = pybind11;

#include <string>
#include <iostream>

using std::string;
using std::to_string;
using std::cout;
using std::endl;

/* ---------------------------------------------------------------------- */

class Example
{
public:
   int number = 7;
   string name = "example object";

   string description () { return name + " " + to_string (number); }
   void modify () { name = "modified object"; }

   py::object getattr (string attr_name);

   Example() { cout << "NEW EXAMPLE" << endl; }
   virtual ~ Example () { cout << "FREE EXAMPLE" << endl; }
};

py::object Example::getattr (string attr_name)
{
    if (attr_name == "something")
    {
       return py::str ("some attribute");
    }
    return  py::str ("");
}

/* ---------------------------------------------------------------------- */

int stored_number = 42;
string stored_text = "abc";

int  get_value () { return stored_number; }
void set_value (int n) { stored_number = n; }

string get_text () { return stored_text; }
void   set_text (string s) { stored_text = s; }

/* ---------------------------------------------------------------------- */

// py::module * sample_module = nullptr;
// py::class_<Example> * example_class = nullptr;

int fce (int a, int b) { return a+b; }

PYBIND11_MODULE (sample, m)
// PYBIND11_EMBEDDED_MODULE (sample, m)
{
    py::class_<Example> (m, "Example")
    // example_class = new py::class_<Example> (m, "Example");
    // (*example_class)

        .def (py::init ())
        .def_readwrite ("number", &Example::number)
        .def_readwrite ("name", &Example::name)
        .def ("description", &Example::description)
        .def ("modify", &Example::modify)
        .def ("__getattr__", &Example::getattr);


    m.def ("fce", &fce);

    m.def ("get_value", &get_value);
    m.def ("set_value", &set_value);

    m.def ("get_text", &get_text);
    m.def ("set_text", &set_text);

    // sample_module = &m;
};

void add_sample_module ()
{
     auto obj = PyInit_sample ();
    _PyImport_SetModuleString ("sample", obj);

     auto fast_calc = py::module_::import ("sample");
     auto result = fast_calc.attr ("fce")(1, 2).cast<int>();
     std::cout << "RESULT IS " << result << std::endl;
}

/* ---------------------------------------------------------------------- */

class GILStateRequest
{
private:
   PyGILState_STATE gil_state;
public:
   GILStateRequest () { gil_state = PyGILState_Ensure (); }
   ~ GILStateRequest () { PyGILState_Release (gil_state); }

};

/* ---------------------------------------------------------------------- */

void test_sample_module ()
{
GILStateRequest request;

string cmd = R"...(
import sample

print ('value =', sample.get_value ())
print ('text =', sample.get_text ())

sample.set_value (7)
sample.set_text ('xyz')

print ('value =', sample.get_value ())
print ('text =', sample.get_text ())

obj = sample.Example()
obj.number = 7
print ('description =', obj.description ())
obj.modify ()
obj.number = 42
print ('description =', obj.description ())

import sys
sys.stdout.flush()
)...";

// cout << "CMD " << endl << cmd << endl << endl;

PyRun_SimpleString (cmd.c_str ());
}

/* ---------------------------------------------------------------------- */

/*
void print_python_repr (PyObject * obj)
{
    PyObject * repr = PyObject_Repr (obj);
    PyObject * str = PyUnicode_AsEncodedString (repr, "utf-8", "~E~");
    const char * bytes = PyBytes_AS_STRING (str);
    cout << "REPR " << bytes << endl;
    Py_XDECREF (str);
    Py_XDECREF (repr);
}
*/

void run_python_string (string cmd)
{
    PyGILState_STATE gil_state = PyGILState_Ensure ();
    PyRun_SimpleString (cmd.c_str());
    PyRun_SimpleString("import sys; sys.stderr.flush()");
    PyGILState_Release (gil_state);
}

void run_python_file (string fileName)
{
    const char * file_name = fileName.c_str ();
    FILE * f = fopen (file_name, "r");
    PyGILState_STATE gil_state = PyGILState_Ensure ();
    PyRun_SimpleFile (f, file_name);
    PyGILState_Release (gil_state);
    fclose (f);
}

/* ---------------------------------------------------------------------- */

void init_python_module ()
{
    Py_Initialize ();
}

void finish_python_module ()
{
    Py_Finalize();
}

/* ---------------------------------------------------------------------- */
