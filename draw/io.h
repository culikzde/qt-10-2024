#ifndef IO_H
#define IO_H

#include <QString>
#include <QGraphicsItem>

/* ---------------------------------------------------------------------- */

const QString toolFormat = "application/x-tool"; // tools from toolbar
const QString shapeFormat = "application/x-shape"; // clipboard, graphics objects
const QString widgetFormat = "application/x-widget"; // widgets from toolbar

const int opaqueKey = 0;

QString colorToString (QColor c);
QString penToString (QPen p);
QString brushToString (QBrush b);

QGraphicsItem * createItem (QString type);
void setupItem (QGraphicsItem * item);

QString itemType (QGraphicsItem * item);
QGraphicsItem * itemFromTool (QString tool, int x, int y);

/* ---------------------------------------------------------------------- */

struct Field
{
    QString name;
    QVariant value;
};

typedef QList <Field> FieldList;

FieldList getFields (QGraphicsItem * item);
void setFields (QGraphicsItem * item, FieldList & list);

/* ---------------------------------------------------------------------- */

class ShapeObject : public QObject
{
   Q_OBJECT
   Q_CLASSINFO ("D-Bus Interface", "org.example.ShapeInterface")
   public:
      ShapeObject (QAbstractGraphicsShapeItem * shape_param) : shape (shape_param) { }

   private:
      QAbstractGraphicsShapeItem * shape;

   public:
      Q_PROPERTY (QString name READ getName)
      QString getName ();

      Q_PROPERTY (QString toolTip READ toolTip WRITE setToolTip)
      QString toolTip () { return shape->toolTip (); }
      void setToolTip (QString value) { return shape->setToolTip (value); }

      Q_PROPERTY (qreal x READ getX WRITE setX)
      qreal getX () { return shape->x (); }
      void setX (qreal value) { return shape->setX (value); }

      Q_PROPERTY (qreal y READ getY WRITE setY)
      qreal getY () { return shape->y (); }
      void setY (qreal value) { return shape->setY (value); }

      Q_PROPERTY (QColor pen READ getPen WRITE setPen)
      QColor getPen () { return shape->pen ().color(); }
      void setPen (QColor value) { shape->setPen (value); }

      Q_PROPERTY (QColor brush READ getBrush WRITE setBrush)
      QColor getBrush () { return shape->brush().color(); }
      void setBrush (QColor value) { shape->setBrush (value); }

      Q_PROPERTY (qreal width READ getWidth WRITE setWidth)
      Q_PROPERTY (qreal height READ getHeight WRITE setHeight)

      Q_INVOKABLE qreal getWidth ()
      {
          int w = 0;
          if (QGraphicsRectItem * e = dynamic_cast < QGraphicsRectItem * > (shape))
          {
             w = e->rect().width();
          }
          if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
          {
             w  = e->rect().width();
          }
          return w;
      }

      Q_INVOKABLE qreal getHeight ()
      {
          int h = 0;
          if (QGraphicsRectItem * e = dynamic_cast < QGraphicsRectItem * > (shape))
          {
             h = e->rect().height();
          }
          if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
          {
             h  = e->rect().width();
          }
          return h;
      }

      Q_INVOKABLE void setWidth (qreal value)
      {
            if (QGraphicsRectItem * e = dynamic_cast < QGraphicsRectItem * > (shape))
            {
                QRectF r = e->rect ();
                r.setWidth (value);
                e->setRect (r);
            }
            if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
            {
                QRectF r = e->rect ();
                r.setWidth (value);
                e->setRect (r);
            }
      }

      Q_INVOKABLE void setHeight (qreal value)
      {
            if (QGraphicsRectItem * e = dynamic_cast < QGraphicsRectItem * > (shape))
            {
                QRectF r = e->rect ();
                r.setHeight (value);
                e->setRect (r);
            }
            if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
            {
                QRectF r = e->rect ();
                r.setHeight (value);
                e->setRect (r);
            }
      }

      Q_PROPERTY (QString penName READ getPenName WRITE setPenName)
      Q_INVOKABLE QString getPenName () { return penToString (shape->pen ()); }
      Q_INVOKABLE void setPenName (QString value) { shape->setPen (QColor (value)); }

      Q_PROPERTY (QString brushName READ getBrushName WRITE setBrushName)
      Q_INVOKABLE QString getBrushName () { return brushToString (shape->brush()); }
      Q_INVOKABLE void setBrushName (QString value) { shape->setBrush (QColor (value)); }
};

QString getShapeName (QAbstractGraphicsShapeItem * shape);

/* ---------------------------------------------------------------------- */

#endif // IO_H
