#include "toolbutton.h"
#include <QMouseEvent>
#include <QDrag>
#include <QMimeData>

ToolButton::ToolButton (QString p_name, QString p_format) :
    name (p_name),
    format (p_format)
{
    setText (name);
    setToolTip (name);

    image = QPixmap (":/icons/" + name + ".svg");
    setIcon (image);
}

void ToolButton::mousePressEvent (QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        QMimeData * mime = new QMimeData;
        mime->setData (format, name.toLatin1());

        QDrag * drag = new QDrag (this);
        drag->setMimeData (mime);
        drag->setPixmap (image);

        Qt::DropAction action = drag->exec (Qt::MoveAction | Qt::CopyAction | Qt::LinkAction);
    }
}
