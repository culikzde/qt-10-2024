
/* qt-view.cc */

#include "qt-view.h"
#include "tree.h"

#include <QMetaMethod>

#include <QPushButton>

/* ---------------------------------------------------------------------- */

QTreeWidgetItem * display_qt_function (QTreeWidgetItem * target, QMetaMethod method)
{
   #if QT_VERSION < 0x050000
      QString text = method.signature ();
   #else
      QString text = method.methodSignature ();
   #endif
   QTreeWidgetItem * node = new QTreeWidgetItem (target);
   node->setText (0, text);
   return node;
}

void display_qt_class (QTreeWidgetItem * target, QObject * obj)
{
   const QMetaObject * cls = obj->metaObject ();

   QTreeWidgetItem * branch = new QTreeWidgetItem (target);
   branch->setText (0, cls->className ());

   int cnt = cls->propertyCount ();
   for (int inx = 0; inx < cnt; inx ++)
   {
      QMetaProperty prop = cls->property (inx);
      QString text = prop.name ();
      text = text + " : " + prop.typeName ();
      try
      {
         text = text + " = " + prop.read (obj).toString ();
      }
      catch (...)
      {
      }
      QTreeWidgetItem * node = new QTreeWidgetItem (branch);
      node->setText (0, text);
      node->setForeground (0, QColor ("orange"));
      node->setToolTip (0, "property");
   }

   cnt = cls->methodCount();
   for (int inx = 0; inx < cnt; inx ++)
   {
      QMetaMethod method = cls->method (inx);
      if (method.methodType() == QMetaMethod::Signal)
      {
         QTreeWidgetItem * node = display_qt_function (branch, method);
         node->setForeground (0, QColor ("green"));
         node->setToolTip (0, "signal");
      }
   }

   for (int inx = 0; inx < cnt; inx ++)
   {
      QMetaMethod method = cls->method (inx);
      if (method.methodType() == QMetaMethod::Slot)
      {
         QTreeWidgetItem * node = display_qt_function (branch, method);
         node->setForeground (0, QColor ("red"));
         node->setToolTip (0, "slot");
      }
   }

   for (int inx = 0; inx < cnt; inx ++)
   {
      QMetaMethod method = cls->method (inx);

      if (method.methodType() != QMetaMethod::Signal && method.methodType() != QMetaMethod::Slot)
      {
         QTreeWidgetItem * node = display_qt_function (branch, method);
         node->setForeground (0, QColor ("blue"));
         node->setToolTip (0, "method");
      }
   }
}

#ifdef USE_UI_LOADER
   #include <QUiLoader>
#endif

void display_qt_classes (QTreeWidget * tree)
{
   #ifdef USE_UI_LOADER
      QUiLoader loader;
      QStringList list = loader.availableWidgets ();
      int len = list.size ();
      for (int inx = 0 ; inx < len; inx++)
      {
          QString name = list [inx];
          QWidget * obj = loader.createWidget (name);
          display_qt_class (tree->invisibleRootItem (), obj);
          delete obj;
      }
   #else
      QObject * obj = new QPushButton;
      display_qt_class (tree->invisibleRootItem (), obj);
      delete obj;
   #endif
   // branch->setExpanded (true);
}

/* ---------------------------------------------------------------------- */

void display_qt_branch (QTreeWidgetItem * target, QObject * obj)
{
    const QMetaObject * cls = obj->metaObject ();
    QString text = obj->objectName() + " : " + cls->className ();

    TreeItem * node = new TreeItem;
    node->setText (0, text);
    target->addChild (node);

    for (QObject * item : obj->children())
        display_qt_branch (node, item);
}

void display_qt_widgets (QTreeWidget * tree)
{
    QObject * widget = tree;
    while (widget->parent() != nullptr)
        widget = widget->parent ();
    display_qt_branch (tree->invisibleRootItem (), widget);
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
