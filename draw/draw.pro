
unix: TARGET = draw-mini.bin

QT += core gui widgets

# --------------------------------------------------------------------------

# CONFIG += DBUS
# CONFIG += JS
# CONFIG += PYTHON
# CONFIG += INVENTOR
# CONFIG += SQL
# CONFIG += SVG
# CONFIG += SOCKET
# CONFIG += WEB_KIT
# CONFIG += WEB_ENGINE
CONFIG += UI_LOADER

CONFIG += PANEL
PANEL {
    CONFIG += SQL_PANEL
    CONFIG += DBUS_PANEL
    CONFIG += JS_PANEL
    CONFIG += SVG_PANEL
    CONFIG += SOCKET_PANEL
    # CONFIG += WEB_KIT_PANEL
    CONFIG += WEB_ENGINE_PANEL
    CONFIG += CHART_PANEL
    CONFIG += VISUALIZATION_PANEL
    CONFIG += QT3D_PANEL
    CONFIG += UI_LOADER_PANEL
}

# --------------------------------------------------------------------------

CONFIG += c++11 precompile_header
PRECOMPILED_HEADER += precompiled.h

HEADERS += precompiled.h
HEADERS += draw.h colorbutton.h toolbutton.h scene.h tree.h property.h win.h
HEADERS += block.h circle.h port.h
HEADERS += io.h xmlio.h jsonio.h
HEADERS += qt-view.h

SOURCES += draw.cc colorbutton.cc toolbutton.cc scene.cc tree.cc property.cc
SOURCES += block.cc circle.cc port.cc
SOURCES += io.cc xmlio.cc jsonio.cc
SOURCES += qt-view.cc

FORMS += draw.ui

RESOURCES += resources.qrc

# --------------------------------------------------------------------------

DBUS {
   QT += dbus
   DEFINES += USE_DBUS
   HEADERS += dbus.h
   SOURCES += dbus.cc
   # dnf install qt5-dbusviewer
}

JS {
    QT += script scripttools qml
    DEFINES += USE_JS
    HEADERS += js.h
    SOURCES += js.cc
    # dnf install qt5-qtscript-devel
}

PANEL {
    DEFINES += USE_PANEL
    HEADERS += panel.h
    SOURCES += panel.cc
}

SQL {
   QT += sql
   DEFINES += USE_SQL
   HEADERS += db.h
   SOURCES += db.cc
}

SVG {
   QT += svg
   QT *= network
   DEFINES += USE_SVG
   HEADERS += svg.h
   SOURCES += svg.cc
   # dnf install qt5-qtsvg-devel
}

SOCKET {
   QT += network
   DEFINES += USE_SOCKET
   HEADERS += socket.h
   SOURCES += socket.cc
}

WEB_KIT {
   QT += webkit webkitwidgets
   HEADERS += html.h
   SOURCES += html.cc
   QMAKE_CXXFLAGS += -D USE_WEB_KIT
   # dnf install qt5-qtwebkit-devel
}

WEB_ENGINE {
   QT += webengine webenginewidgets
   HEADERS += html.h
   SOURCES += html.cc
   QMAKE_CXXFLAGS += -D USE_WEB_ENGINE
   # dnf install qt5-qtwebengine-devel
}

PYTHON {
   DEFINES += USE_PYTHON
   HEADERS += python-engine.h
   SOURCES += python-engine.cc

   CONFIG *= link_pkgconfig
   PKGCONFIG += python3-embed
   # dnf install python3-devel
}

INVENTOR {
   DEFINES += USE_INVENTOR
   HEADERS += inventor.h
   SOURCES += inventor.cc
   LIBS += -lSoQt -lCoin
   # DEFINES += SOQT_DLL COIN_DLL
   # DEFINES += SOQT_NOT_DLL COIN_NOT_DLL

   CONFIG *= link_pkgconfig
   PKGCONFIG += SoQt Coin

   # dnf install SoQt-devel
   # apt install libsoqt520-dev
   # pacman -S mingw-w64-i686-soqt
}

UI_LOADER {
    QT += uitools
    DEFINES += USE_UI_LOADER
    # dnf install qt5-qttools-devel
    # dnf install q5-qttools-static
}
# --------------------------------------------------------------------------

win32 {
   CONFIG -= WEB_KIT_PANEL
   gcc {
      CONFIG -= WEB_ENGINE_PANEL
   }
   SOCKET {
      CONFIG -= VISUALIZATION_PANEL
   }
}

greaterThan(QT_MAJOR_VERSION, 6) {
   CONFIG -= JS_PANEL
   CONFIG -= WEBENGINE_PANEL
   CONFIG -= WEBKIT_PANEL
   CONFIG -= CHART_PANEL
   CONFIG -= VISUALIZATION_PANEL
}

# --------------------------------------------------------------------------

SQL_PANEL {
    QT += sql
    DEFINES += SQL_PANEL
}

DBUS_PANEL {
    QT += dbus
    DEFINES += DBUS_PANEL
}

JS_PANEL {
    QT += script scripttools
    DEFINES += JS_PANEL
    HEADERS *= js.h
    SOURCES *= js.cc
    # dnf install qt5-qtscript-devel
}

SVG_PANEL {
    QT += svg network
    greaterThan(QT_MAJOR_VERSION, 5): QT += svgwidgets
    DEFINES += SVG_PANEL
    # dnf install qt5-qtsvg-devel
}

SOCKET_PANEL {
    QT += network
    DEFINES += SOCKET_PANEL
}

WEB_ENGINE_PANEL {
    QT += webengine webenginewidgets
    DEFINES += WEBENGINE_PANEL
    # dnf install qt5-qtwebengine-devel
}

WEB_KIT_PANEL {
    QT += webkit webkitwidgets
    DEFINES += WEBKIT
    # dnf install qt5-qtwebkit-devel
}

CHART_PANEL {
    QT += charts
    DEFINES += CHART_PANEL
    # dnf install qt5-qtcharts-devel
}

VISUALIZATION_PANEL {
    QT += datavisualization
    DEFINES += VISUALIZATION_PANEL
    # dnf install qt5-qtdatavis3d-devel
}

QT3D_PANEL {
    QT += 3dcore 3dextras 3drender 3dinput
    DEFINES += QT3D_PANEL
    # dnf install qt5-qt3d-devel
}

UI_LOADER_PANEL {
    QT *= uitools
    DEFINES += UI_LOADER_PANEL
    # dnf install qt5-qttools-devel
    # dnf install q5-qttools-static
}

# --------------------------------------------------------------------------

gcc {
   QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-parameter -Wno-unused-variable -Wno-parentheses
}

msvc {
   QMAKE_CXXFLAGS_WARN_ON -= -w34100 -w34189
   QMAKE_CXXFLAGS_WARN_OFF += -wd4100 -wd4189
}

# unix {
# # https://stackoverflow.com/questions/19066593/copy-a-file-to-build-directory-after-compiling-project-with-qt
# copydata.commands = $(COPY_DIR) $$PWD/data/{draw.html,easyui.html,tree.json,table.json} $$OUT_PWD
# first.depends = $(first) copydata
# export(first.depends)
# export(copydata.commands)
# QMAKE_EXTRA_TARGETS += first copydata
# }

# --------------------------------------------------------------------------
