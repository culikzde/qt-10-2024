#ifndef QT_INVENTOR_H
#define QT_INVENTOR_H

#include <Inventor/Qt/SoQt.h>
#include <Inventor/nodes/SoSeparator.h>

class QTreeWidget;
class QTreeWidgetItem;
class QTabWidget;
class PropertyTable;

void inventorScene (QTreeWidget * tree, QTabWidget * tabs, PropertyTable * prop);

void displaySoNode (QTreeWidgetItem * target, SoNode * node); // show tree branch
void showSoNode (PropertyTable * prop, SoNode * node); // show properties

#endif // QT_INVENTOR_H
