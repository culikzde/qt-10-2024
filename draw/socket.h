
/* socket.h */

#ifndef SOCKET_H
#define SOCKET_H

#include <QTcpServer>
#include <QTextEdit>
#include <QTextEdit>
#include "win.h"

class SocketReceiver : public QObject
{

   public:
      SocketReceiver (Win * win_param, int port_param = 1234);
      ~ SocketReceiver ();

      void run();
   private:
      Win * win;
      QTextEdit * info ;

      int port;
      QTcpServer * server;
      QTcpSocket * socket;

      void print (QString s);
      void connection ();
};

#endif // SOCKET_H
