
/* js.h */

#ifndef JS_H
#define JS_H

#include <QTreeWidget>
#include <QTreeWidgetItem>

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QAbstractGraphicsShapeItem>
#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>

#include <QTextEdit>
#include <QCompleter>

#include <QScriptValue>

/* ---------------------------------------------------------------------- */

class JsEdit : public QTextEdit
{
   Q_OBJECT

   public:
      explicit JsEdit (QWidget * parent, QTreeWidget * tree_param, QTextEdit * info_param, QGraphicsScene * scene_param);

    // drag and drop
    protected:
        bool canInsertFromMimeData (const QMimeData * data) const override;
        void insertFromMimeData (const QMimeData * data) override;

    // text completion
    protected:
       void keyPressEvent (QKeyEvent *e) override;
       void focusInEvent (QFocusEvent *e) override;

    private slots:
       void insertCompletion (const QString &completion);

    private:
       QCompleter * completer;

    private:
       QString textUnderCursor() const;
       void setCompleter (QCompleter * c);
       void setCompletion (QStringList c);

   // Java Script
   private:
      QTreeWidget * tree;
      QTextEdit * info;
      QGraphicsScene * scene;

      void showItem (QTreeWidgetItem * above, QString name, QScriptValue value, int level);
      void showBranch (QTreeWidgetItem * above, QScriptValue branch, int level);
      void addToCompletion (QScriptValue branch);

      void execute (QScriptEngine & engine);

   public:
      void setTree (QTreeWidget * p_tree);
      void setInfo (QTextEdit * p_info);

   public slots:
      void debug ();
      void run ();
};

/* ---------------------------------------------------------------------- */

#endif // JS_H
