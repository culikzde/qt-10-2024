#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QTreeWidget>

#include "scene.h"
#include "win.h"

#ifdef USE_JS
   #include "js.h"
#endif

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow, public Win
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void showProperty (QString name, QVariant value) override;
    void showProperties (QGraphicsItem * item) override;
    void showInfo (QString s) override;

    void refreshTree () override;
    void refreshTreeName (QGraphicsItem * item, QString name) override;
    void refreshNewTreeItem (QGraphicsItem * item) override;

    QMainWindow * getMainWindow () override;
    QStatusBar * getStatusBar () override;
    Tree * getTree () override;
    PropertyTable * getPropTable () override;
    QTextEdit * getInfo () override;
    QGraphicsScene * getScene () override;

    void loadFile (QString fileName, bool json);
    void saveFile (QString fileName, bool json);

private slots:
    void on_tree_itemSelectionChanged();
    void on_tree_itemDoubleClicked (QTreeWidgetItem *item, int column);

    void on_inventor_tree_clicked (QTreeWidgetItem * node, int column);

    void on_actionOpen_triggered ();
    void on_actionSave_triggered ();
    void on_actionQuit_triggered ();

    void on_actionCopy_triggered ();
    void on_actionPaste_triggered ();

    void on_actionTree_triggered ();
    void on_actionSvg_triggered ();

    void on_actionTransfer_triggered ();
    void on_actionCall_triggered ();
    void on_actionScript_triggered ();
    void on_actionPython_triggered ();

private:
    Ui::MainWindow * ui;

    Tree * tree;
    PropertyTable * prop;
    QGraphicsScene * scene;
    QTextEdit * info;

    #ifdef USE_JS
      QTreeWidget * js_tree = nullptr;
      JsEdit * js_edit = nullptr;
    #endif

    #ifdef USE_PYTHON
      QTextEdit * python_edit = nullptr;
    #endif

    #ifdef USE_INVENTOR
      QTreeWidget * inventor_tree = nullptr;
    #endif

    QTreeWidget * qt_tree = nullptr;

    void createPalette ();
    void addTab (QWidget * widget, QString title);
    void addLeftTool (QWidget * widget, QString title);
    void addRightTool (QWidget * widget, QString title);
    void showTool (QWidget * widget);
    QWidget * createRunBox (QWidget * widget, QAction * action);

public:

};
#endif // MAINWINDOW_H
