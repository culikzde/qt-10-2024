#ifndef BLOCK_H
#define BLOCK_H

#include <QGraphicsRectItem>

class Block : public QObject, public QGraphicsRectItem // first QObject
{
    Q_OBJECT

#ifdef USE_DBUS
    Q_CLASSINFO ("D-Bus Interface", "org.example.BlockInterface")
#endif

public:
    QString name;
    Q_PROPERTY (QString name MEMBER name SCRIPTABLE true)
    Q_SCRIPTABLE void move (int x, int y);
    Q_SCRIPTABLE void setColor (QString color);
    Q_SCRIPTABLE void setBorder (QString color, int width = 1);

public:
    Block (bool add_circles = false);

    #ifdef DRAG_AND_DROP
    void dragEnterEvent (QGraphicsSceneDragDropEvent *event) override;
    void dropEvent (QGraphicsSceneDragDropEvent *event) override;
    #endif
};

#ifdef DRAG_AND_DROP
void modifyColor (QAbstractGraphicsShapeItem * shape,
                  QGraphicsSceneDragDropEvent * event);

void addObject (QGraphicsItem * shape,
                QGraphicsSceneDragDropEvent * event);
#endif


#endif // BLOCK_H
