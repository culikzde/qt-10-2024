#include "colorbutton.h"

#include <QDrag>
#include <QMimeData>
#include <QMouseEvent>
#include <QPainter>

ColorButton::ColorButton(QString p_name) :
    name (p_name),
    color (QColor (p_name))
{
    setText (name);
    setToolTip (name);
    setIcon (getImage ());
}

QPixmap ColorButton::getImage ()
{
    int size = 24;
    QPixmap image (size, size);
    image.fill (Qt::transparent);
    QPainter painter (&image);
    painter.setPen (Qt::NoPen);
    painter.setBrush (color);
    painter.drawEllipse (0, 0, size, size);
    return image;
}

void ColorButton::mousePressEvent (QMouseEvent * event)
{
    if (event->button() == Qt::LeftButton )
    {
         QMimeData * mime = new QMimeData;
         mime->setText (name);
         mime->setColorData (color);

         QDrag * drag = new QDrag (this);
         drag->setMimeData (mime);
         drag->setPixmap (getImage ());

         Qt::DropAction dropAction = drag->exec (Qt::MoveAction | Qt::CopyAction | Qt::LinkAction);
    }
}
