#include "tree.h"
#include "ui_tree.h"
#include <QApplication>
#include <QDir>
#include <QDateTime>
#include <QTextEdit>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tabs->clear ();
}

MainWindow::~MainWindow()
{
    delete ui;
}

QString fmt (long n)
{
    QString s = QString::number (n);
    while (s.length() < 12)
        s = " " + s;
    s = s.mid(0, 3) + " " + s.mid(3, 3) + " " + s.mid(6, 3) + " " + s.mid(9, 3);
    return s;
}

void MainWindow::displayDir (QTreeWidgetItem * target, QString path)
{
    QDir dir (path);
    QFileInfoList list = dir.entryInfoList
                         (QDir::AllEntries | QDir::NoDot | QDir::NoDotDot,
                          QDir::DirsFirst | QDir::Name);
    for (QFileInfo info : list)
    {
        QTreeWidgetItem * item = new QTreeWidgetItem;

        item->setText (0, info.fileName ());
        item->setToolTip (0, info.filePath ());
        if (info.isDir ())
        {
            item->setForeground (0, QColor ("red"));
            displayDir (item, info.filePath ());
        }
        else
        {
            item->setForeground (0, QColor ("blue"));
            item->setTextAlignment (1, Qt::AlignRight);
            item->setText (1, fmt (info.size ()));
            item->setText (2, info.lastModified().toString ("yyyy-MM-dd HH:mm:ss"));
            // #include <QDateTime>
        }
        target->addChild (item);
    }
}

void MainWindow::on_actionRun_triggered()
{
   ui->tree->clear ();
   // ui->tree->header()->hide ();
   ui->tree->setHeaderLabels (QStringList () << "name" << "size" << "date");

   QDir dir0 ("..");
   QDir dir  (dir0.absolutePath ());

   QTreeWidgetItem * top = new QTreeWidgetItem;
   top->setText (0, dir.dirName ());
   top->setToolTip (0, dir.absolutePath ());
   top->setForeground (0, QColor ("red"));
   ui->tree->addTopLevelItem (top);

   displayDir (top, dir.absolutePath ());

   // top->setExpanded (true);
   ui->tree->expandAll();
}

void MainWindow::openEditor (QString fileName)
{
    QFileInfo info (fileName);
    QString fullName = info.absoluteFilePath();

    QTextEdit * edit = nullptr;
    if (editors.contains (fullName))
    {
        edit = editors [fullName];
    }
    else
    {
        edit = new QTextEdit;
        editors [fullName] = edit;

        int inx = ui->tabs->addTab (edit, info.fileName());
        ui->tabs->setTabToolTip (inx, fullName);

        QFile f (fullName);
        if (f.open (QFile::ReadOnly))
        {
            QString text = f.readAll();
            edit->setText (text);
            f.close ();
        }
    }

    ui->tabs->setCurrentWidget (edit);
}

void MainWindow::on_tree_itemDoubleClicked (QTreeWidgetItem * item, int column)
{
    openEditor (item->toolTip (0));
}

void MainWindow::on_actionQuit_triggered()
{
    close ();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}

